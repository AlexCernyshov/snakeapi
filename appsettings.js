const fs = require('fs');
const path = require('path');

const appSettingsPath = path.join(__dirname, 'appsettings.json');
console.log(appSettingsPath)
// Читаем содержимое файла appsettings.json с использованием синхронного метода
const data = fs.readFileSync(appSettingsPath, 'utf8');
// Преобразуем JSON строку в объект
const config = JSON.parse(data);

const dbConnectionSettings = config.dbConnectionSettings;

module.exports = {
    dbConnectionSettings: dbConnectionSettings,
};