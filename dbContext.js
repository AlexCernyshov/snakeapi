const appSettings = require('./appsettings');
const { Client } = require('pg'); 

const dbConnectionSettings = appSettings.dbConnectionSettings;
const { host, user, port, password, database } = dbConnectionSettings;

const client = new Client({ 
    host: host,
    user: user,
    port: port,
    password: password, 
    database: database
}); 


client.connect(); 
function getAll() {
    return new Promise((resolve, reject) => {
        client.query(`SELECT * FROM public."snake"`, (err, res) => {
            
            if (err) {
                console.error(err.message);
                reject(err); // отклоняем промис в случае ошибки
            } else {
                console.log(res.rows);
                resolve(res.rows); // разрешаем промис с результатом запроса
            }
        });
    });
}

function insert(model){
    console.log('Сохраняем')
    console.log(model)
    const query = `INSERT INTO public.snake(playername, score) VALUES ('${model.playerName}', ${model.score})`;
    console.log(query);
    client.query(query, (err, res) => {
        
        if (err) {
            console.error(err.message);
            
        } else {
            console.log(res.rows);
        }
    });
}

module.exports = {
    getAll,
    insert
}

function getById(tableName, id) {
    // Реализация функции getById
}